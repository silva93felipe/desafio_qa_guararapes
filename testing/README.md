<h1 align="center">Desafio QA Guararapes</h1>

## Índice
- [Teste manual do backend](#-teste-manual-do-backend)

- [Teste manual do frontend](#-teste-manual-do-frontend)

- [Testes automatizados do beckend e frontend](#-testes-automatizados-do-beckend-e-frontend)



---

## Teste manual do Backend - Api de Materiais

### Material - Controller


**GETALL** - Funcionando normalmente, onde podemos escolher se queremos só matéria prima ou não. Retornou ***"Status 200 Exibe o material buscado"***

--- 
**POST** - Não consegui simular o cadastrar via API onde apresenta a seguinte mensagem ***"Status 500"***:
```
	{
	"timestamp": "2021-05-10T00:10:26.265+00:00",
	"status": 500,
	"error": "Internal Server Error",
	"message": "",
	"path": "/material"
	}
```

---

**GET** - Funcionando normalmente. Consigo pesquisar qualquer produto que esteja cadastrado retornando ***"Status 200 Exibe o material buscado"*** e quando pesquiso produto que não existe ele me retorna ***"Status 400 Erro. Não foi possível encontrar o registro buscado"***. Resultados esperados.

---		
**PUT** - No editar me deparei com uma situação. Ao tentar editar ele me retornava um ***"Status 400"*** com a seguinte mensagem:
```
    [
        {
        "campo": "estoqueForm",
        "mensagemDeErro": "não deve ser nulo"
        }
    ]
```
Consultei os cadastros existentes dos materiais e não vi esse campo ***"estoqueForm"***, percebi então que esse campo ***"estoqueForm"*** era o  ***"quantidadeEmEstoque"*** e para que desse certo
tive que acresentar esse campo ***"estoqueForm"*** no lugar ***"quantidadeEmEstoque"***  processo onde ficou da seguinte forma:
```		{
		  "id": 6,
		  "nome": "calcas",
		  "estoqueForm": 1, 
		  "materiaisDeProducaoDTO": [
				{
			  "id": 1,
			  "nome": "tecido",
			  "materiaisDeProducaoDTO": null,
			  "quantidadeEmEstoque": 6
				}
				],
		"quantidadeEmEstoque": 0
		}
```
Isso tanto para materias-primas ou não, onde após isso me retornou ***"Status 200 Atualiza o material desejado"***

---

**DELETE** - Funcionando normalmente. Coloquei o ID desejado para exclusão e após o a execução retornou ***"Status 200 Remove o material desejado"***, e quando tento excluir um que não está cadastrado ele me retornar ***"Status 400 Erro. Não foi possível excluir o registro"***. Realizei novamente a pesquisa, tanto geral quanto individual e o produto deletado não constava mais.

---

- ### Ordem-controller

**GET** - Funcionando normalmente. Retornou ***"Status 200 Busca ordens de produção"*** devido ter ordem cadastradas.

---

**POST** - Não consegui simular

---

## Teste manual do Frontend

1. ***(Aceito)*** Crie um crud de criação de ordens de produção, que podem ser criadas a partir de um produto final caso todas as suas matérias primas possuam estoque.
	- Cadastrar:
		1º Tela antes de cadastrar:
		<img src="img_test/apos_deletar.png">

		2º Tela de cadastro:
		<img src="img_test/tela_de_cadastrar.png">

		3º Tela após cadastro:
		<img src="img_test/tela_apos_cadastro.png">

	- Editar:

		1º Item antes de editar:
		<img src="img_test/antes_de_editar.png">

		2º Tela de editar:
		<img src="img_test/tela_de_editar.png">
		***Nessa tela vi que não está trazendo a quantidade dos itens que já estão adicionados, mas quando adiciona ele mostra. (Ponto para melhoria)***

		3º Item depois de editado:
		<img src="img_test/apos_editado.png">

	- Deletar:

		1º Lista antes de deletar o item 6:
		<img src="img_test/tela_produto_final.png">

		2º Lista após de deletar o item 6:
		<img src="img_test/apos_deletar.png">

2. ***(Aceito)*** Crie um frontend em SPA para listar os materiais considerados produtos finais (camisas, calças, vestidos, etc)
<img src="img_test/tela_produto_final.png">

3. ***(Aceito)*** Nessa listagem deve haver um botão de liberar ordem de produção, porém esse botão só deve funcionar no caso em que o produto possua todas as suas matérias primas em estoque.

  	Criar a ordem de produção:

	1º Utilizei um produto final com saldo nos itens da matéria-prima:
	<img src="img_test/gerar_ordem_12.png">

	2º Cliquei no botão "ordem de produção":
	<img src="img_test/gerar_ordem_13.png">
	<img src="img_test/gerar_ordem_14.png">

	3º Clicar no botão de listar ordens de produção foi exibida a ordem cadastrada:
	<img src="img_test/gerar_ordem_15.png">

	<img src="img_test/gerar_ordem_16.png">

	4º Voltei a página anterior e a quantidade de cada item do produto final tinha diminuído e a quantidade do produto final aumentado:
	<img src="img_test/gerar_ordem_17.png">

---

4. ***(Aceito)*** Teste da crítica quando uma das matérias-prima não tem saldo para fazer a ordem de produção.
	
	1º Adicionei uma matéria-prima sem saldo a um produto final.
	<img src="img_test/gerar_ordem_1.png">

	2º Cliquei no botão de "ordem de produção" para gerar:
	<img src="img_test/gerar_ordem_2.png">

	3º Ao tentar salvar apresenta a crítica:
	<img src="img_test/gerar_ordem_3.png">

---

## Testes automatizados do beckend e frontend

Para o uso dos teste automatizados seguir o seguinte caminho:
```
cd testing/

npm install

npm run test
```
Vai rodar tanto o teste do backend que é o arquivo ***api.test.js***, quanto do frontend que é o arquivo ***front.test.js***

