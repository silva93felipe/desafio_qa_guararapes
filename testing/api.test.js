const Axios = require("axios");

// Teste em material-controller

// Listar todos os materias
test('Api deve listar todos os itens.', async () => {
    let response = await Axios.get("http://localhost:8080/material");
    expect(response.status).toBe(200);
    
});

// Pesquisar por um material em especifico
test('Api deve pesquisar pelo item.', async () => {
    let response = await Axios.get("http://localhost:8080/material/1");
    expect(response.status).toBe(200);
});

// Cadastra novos materiais - Aqui dá status 500 e com isso não consigo fazer o teste.
// test('Api deve cadastrar novo material.', async () => {
//     let response = await Axios.post("http://localhost:8080/material",{
//         id: 11,
//         nome: "Venho do teste",
//         materiaisDeProducaoDTO: null,
//         estoqueForm: 5
//     });
//     expect(response.status).toBe(201);
// });

// Editar materiais
test('Api deve editar um item em questão.', async () => {
    let response = await Axios.put("http://localhost:8080/material/1", {
        nome: "Editado do testes",
        materiaisDeProducaoDTO: null,
        estoqueForm: 5
    })
    expect(response.status).toBe(200);
}); 

// Deletar materiais
test('Api deve deletar o item em questão', async () => {
    let response = await Axios.delete("http://localhost:8080/material/4/")
    expect(response.status).toBe(200);
});


// Teste em ordem-controller

// Listar todas as ordens de produtos
test('Api deve listar todas as ordens de produtos.', async () => {
    let response = await Axios.get("http://localhost:8080/ordem");
    expect(response.status).toBe(200);
});

// Cadastra novas ordens de produtos
// test('Api de cadastrar nova ordem de produto', async () => {
//     let response = await Axios.post("http://localhost:8080/ordem",{
//        
//     });
//     expect(response.status).toBe(201);
// });